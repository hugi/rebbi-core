package is.rebbi.core.template;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

/**
 * Tests for TemplateSimple
 */

public class TestTemplateSimple {

	@Test
	public void parse() {
		Map<String, Object> vars = new HashMap<String, Object>();
		vars.put( "var-1", "Hugi Þórðarson" );
		vars.put( "var-2", "Chuck Norris" );
		testOne( "${var-1}", vars, true, "Hugi Þórðarson" );
		testOne( "${var-1}${var-2}", vars, true, "Hugi ÞórðarsonChuck Norris" );
		testOne( "Halló ${var-1}, hvernig hefur þú það í dag?", vars, true, "Halló Hugi Þórðarson, hvernig hefur þú það í dag?" );
		testOne( "${var-1} borðar ${var-2} í morgunmat.", vars, false, "Hugi Þórðarson borðar Chuck Norris í morgunmat." );
		testOne( "${var-1} borðar ${var-2} í ${var-3}.", vars, true, "Hugi Þórðarson borðar Chuck Norris í ." );
		testOne( "${var-1} borðar ${var-2} í ${var-3}.", vars, false, "Hugi Þórðarson borðar Chuck Norris í ${var-3}." );
	}

	private void testOne( String templateString, Map<String, Object> vars, boolean removeUnsetVariables, String expected ) {
		TemplateSimple template = new TemplateSimple();
		template.setTemplateString( templateString );
		template.setRemoveUnsetVariablesFromTemplate( removeUnsetVariables );
		template.putAll( vars );
		String result = template.parse();
		assertEquals( expected, result );
	}
}