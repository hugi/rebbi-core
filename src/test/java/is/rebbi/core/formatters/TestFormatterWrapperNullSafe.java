package is.rebbi.core.formatters;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.jupiter.api.Test;

/**
 * Tests for FormatterWrapperNullSafe
 */

public class TestFormatterWrapperNullSafe {

	@Test
	public void testFormatNullDate() {
		FormatterWrapperNullSafe f = new FormatterWrapperNullSafe( new SimpleDateFormat( "dd.MM.yyyy" ) );
		assertEquals( "", f.format( null ) );

		Date date = new Date( 111, 0, 1 );
		String actual = f.format( date );
		assertEquals( "01.01.2011", actual );
	}
}