package is.rebbi.core.formatters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;

import org.junit.jupiter.api.Test;

public class TestDateTimeFormatterWrapper {

	@Test
	public void format() {
		DateTimeFormatterWrapper f = new DateTimeFormatterWrapper( DateTimeFormatter.ISO_DATE );

		assertEquals( "", f.format( null ) );
		assertEquals( "2021-12-31", f.format( LocalDate.of( 2021, 12, 31 ) ) );

		try {
			assertEquals( LocalDate.of( 2021, 12, 31 ), LocalDate.from( (TemporalAccessor)f.parseObject( "2021-12-31" ) ) );
		}
		catch( ParseException e ) {
			fail();
		}
	}
}