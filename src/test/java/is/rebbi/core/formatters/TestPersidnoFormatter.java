package is.rebbi.core.formatters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.text.ParseException;

import org.junit.jupiter.api.Test;

import is.rebbi.core.util.PersidnoUtilities;

/**
 * Tests for PersidnoFormatter.
 */

public class TestPersidnoFormatter {

	@Test
	public void parsing() throws ParseException {
		String input = "091179-4829";
		String expected = "0911794829";

		PersidnoFormatter formatter = new PersidnoFormatter();
		assertEquals( formatter.parseObject( input ), expected );
	}

	@Test
	public void formatting() {
		String input = "0911794829";
		String expected = "091179-4829";

		PersidnoFormatter formatter = new PersidnoFormatter();
		assertEquals( formatter.format( input ), expected );
		assertEquals( formatter.format( null ), "" );
	}
	
	@Test
	public void isStandardHyphenatedForm() {
		assertTrue( PersidnoUtilities.isStandardHyphenatedForm( "091179-4829" ) );
		assertFalse( PersidnoUtilities.isStandardHyphenatedForm( "0911794829" ) );
		assertFalse( PersidnoUtilities.isStandardHyphenatedForm( "" ) );
		assertThrows( NullPointerException.class, () -> {
			PersidnoUtilities.isStandardHyphenatedForm( null );
		} );
	}
}