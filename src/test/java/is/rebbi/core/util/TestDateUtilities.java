package is.rebbi.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class TestDateUtilities {

	@Test
	public void ageAtDate() {
		assertEquals( 42, DateUtilities.ageAtDate( LocalDate.of( 1979, 11, 9 ), LocalDate.of( 2022, 11, 8 ) ) );
		assertEquals( 43, DateUtilities.ageAtDate( LocalDate.of( 1979, 11, 9 ), LocalDate.of( 2022, 11, 9 ) ) );
		assertEquals( 43, DateUtilities.ageAtDate( LocalDate.of( 1979, 11, 9 ), LocalDate.of( 2022, 11, 10 ) ) );
	}
}