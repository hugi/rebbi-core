package is.rebbi.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

/**
 * Tests for USPersidnoUtilities.
 */

public class TestPersidnoUtilities {

	@Test
	public void cleanupPersidno() {
		assertTrue( PersidnoUtilities.cleanupPersidno( "091179 4829" ).equals( "0911794829" ) );
		assertTrue( PersidnoUtilities.cleanupPersidno( "091179-4829" ).equals( "0911794829" ) );
		assertTrue( PersidnoUtilities.cleanupPersidno( "0911794829" ).equals( "0911794829" ) );
		assertTrue( PersidnoUtilities.cleanupPersidno( "09" ).equals( "09" ) );
		assertFalse( PersidnoUtilities.cleanupPersidno( "091179.4829" ).equals( "0911794829" ) );
	}

	@Test
	public void birthyearFromPersidno() {
		assertTrue( PersidnoUtilities.birthYearFromPersidno( "091179 4829" ) == 1979 );
		assertTrue( PersidnoUtilities.birthYearFromPersidno( "190876-3659" ) == 1976 );
		assertTrue( (PersidnoUtilities.birthYearFromPersidno( "5703003340" ) == null) );
		assertFalse( PersidnoUtilities.birthYearFromPersidno( "0911794829" ) == 1999 );
	}

	@Test
	public void birthMonthFromPersidno() {
		assertTrue( PersidnoUtilities.birthMonthFromPersidno( "091179 4829" ) == 11 );
		assertTrue( PersidnoUtilities.birthMonthFromPersidno( "190876-3659" ) == 8 );
		assertTrue( PersidnoUtilities.birthMonthFromPersidno( "5703003340" ) == null );
		assertFalse( PersidnoUtilities.birthMonthFromPersidno( "0911794829" ) == 10 );
	}

	@Test
	public void birthDayFromPersidno() {
		assertTrue( PersidnoUtilities.birthDayFromPersidno( "091179 4829" ) == 9 );
		assertTrue( PersidnoUtilities.birthDayFromPersidno( "190876-3659" ) == 19 );
		assertTrue( PersidnoUtilities.birthDayFromPersidno( "5703003340" ) == null );
		assertFalse( PersidnoUtilities.birthDayFromPersidno( "0911794829" ) == 8 );
	}

	@Test
	public void formatPersidno() {
		assertTrue( PersidnoUtilities.formatPersidno( "091179 4829" ).equals( "091179-4829" ) );
		assertTrue( PersidnoUtilities.formatPersidno( "0911794829" ).equals( "091179-4829" ) );
		assertTrue( PersidnoUtilities.formatPersidno( "570300-3340" ).equals( "570300-3340" ) );
	}

	@Test
	public void formatPersidnoWithDelimiter() {
		assertTrue( PersidnoUtilities.formatPersidno( "091179 4829", "-" ).equals( "091179-4829" ) );
		assertTrue( PersidnoUtilities.formatPersidno( "0911794829", "-" ).equals( "091179-4829" ) );
		assertTrue( PersidnoUtilities.formatPersidno( "570300-3340", "-" ).equals( "570300-3340" ) );
	}

	@Test
	public void isIndividualPersidno() {
		assertTrue( PersidnoUtilities.isIndividualPersidno( "0911794829" ) );
		assertFalse( PersidnoUtilities.isIndividualPersidno( "5703003340" ) );
		assertFalse( PersidnoUtilities.isIndividualPersidno( null ) );
	}

	@Test
	public void isCompanyPersidno() {
		assertTrue( PersidnoUtilities.isCompanyPersidno( "5703003340" ) );
		assertFalse( PersidnoUtilities.isCompanyPersidno( "0911794829" ) );
		assertFalse( PersidnoUtilities.isCompanyPersidno( null ) );
	}

	@Test
	public void validatePersidno() {
		assertTrue( PersidnoUtilities.validatePersidno( "1708755839" ) );
		assertTrue( PersidnoUtilities.validatePersidno( "0911794829" ) );
		assertTrue( PersidnoUtilities.validatePersidno( "2007765699" ) );

		assertFalse( PersidnoUtilities.validatePersidno( null ) );
		assertFalse( PersidnoUtilities.validatePersidno( "AAA" ) );
		assertFalse( PersidnoUtilities.validatePersidno( "BBBBBBBBBB" ) );

		// INN-702 this company persidno is failing
		assertTrue( PersidnoUtilities.validatePersidno( "4612911399" ) );

		// Look into these.
		// assertFalse( USStringUtilities.validatePersidno( "0000000000" ) );
		// assertFalse( USStringUtilities.validatePersidno( "1111111111" ) );
	}

	@Test
	public void birthdateFromPersidno() {
		LocalDate expectedBirthdate = LocalDate.of( 1979, 11, 9 );
		LocalDate hugiBirthDate = PersidnoUtilities.birthdateFromPersidno( "0911794829" );
		assertEquals( hugiBirthDate, expectedBirthdate );
	}

	/**
	 * Note: Unless we find a way to keep me 29 forever, this test will eventually fail. We need to find another way to provide test data.
	 */
	@Test
	public void ageFromPersidno() {
		Integer n = PersidnoUtilities.ageFromPersidno( "0911794829", LocalDate.of( 2011, 1, 1 ) );
		assertTrue( n == 31 );
	}

	/**
	 * Note: Unless we find a way to keep me 29 forever, this test will eventually fail. We need to find another way to provide test data.
	 */
	@Test
	public void nextBirthday() {
		LocalDate birthday = null;

		birthday = PersidnoUtilities.nextBirthday( LocalDate.of( 2012, 1, 1 ), "0911794829" );
		assertEquals( LocalDate.of( 2012, 11, 9 ), birthday );

		birthday = PersidnoUtilities.nextBirthday( LocalDate.of( 2012, 5, 1 ), "1203833029" );
		assertEquals( LocalDate.of( 2013, 3, 12 ), birthday );
	}
}