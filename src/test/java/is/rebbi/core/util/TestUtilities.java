package is.rebbi.core.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utility code for unit tests
 */

public class TestUtilities {

	private static final int readBufferSize = 8192;
	private static final String DIGEST_ALGORITHM = "MD5";

	/**
	 * Resolves the document physical path for documents located in the class folder.
	 * Note, folder is always the same as the caller Class folder
	 *
	 * @param class thats used to locate the file
	 * @param fileName name of the file
	 * @return full document path for file
	 */
	public static String documentPath( Class<?> caller, String fileName ) {
		return classPath( caller ) + fileName;
	}

	/**
	 * Retrieves the physical directory path of the Class
	 *
	 * @param caller Class
	 * @return directory path of the class
	 */
	public static String classPath( Class<?> caller ) {
		final URL location;
		String loc = caller.getName().replace( '.', File.separatorChar ) + ".class";

		final ClassLoader loader = caller.getClassLoader();
		if( loader == null ) {
			return null;
		}
		else {
			location = loader.getResource( loc );
			loc = location.getPath();
		}
		int fileIdx = loc.lastIndexOf( File.separatorChar );
		if( fileIdx >= 0 ) {
			loc = loc.substring( 0, fileIdx + 1 );
		}
		return loc;
	}

	/**
	 * Calculates a digest from a file.
	 *
	 * @param inputFile full path to file
	 * @return a digest string of the files contents
	 * @throws NoSuchAlgorithmException if the algorithm, used for digest calculations, is not found
	 * @throws IOException if an I/O error occurs
	 */
	public static String streamDigest( String inputFile ) {
		try {
			FileInputStream fis = new FileInputStream( inputFile );
			return streamDigest( fis );
		}
		catch( Exception e ) {
			return null;
		}
	}

	/**
	 * Calculates a digest from an input stream.
	 *
	 * @param inputFile full path to file
	 * @return a digest string of the files contents
	 * @throws NoSuchAlgorithmException if the algorithm, used for digest calculations, is not found
	 * @throws IOException if an I/O error occurs
	 */
	public static String streamDigest( InputStream inputStream ) throws NoSuchAlgorithmException, IOException {
		MessageDigest digest = MessageDigest.getInstance( DIGEST_ALGORITHM );
		byte[] buffer = new byte[readBufferSize];
		int read = 0;
		while( (read = inputStream.read( buffer )) > 0 ) {
			digest.update( buffer, 0, read );
		}
		byte[] md5sum = digest.digest();
		BigInteger bigInt = new BigInteger( 1, md5sum );
		return bigInt.toString( 16 );
	}

	/**
	 * Saves a stream to a physical file
	 * @param inStream stream to save
	 * @param fileName name of file to save to
	 * @throws IOException if an I/O error occurs
	 */
	public void saveFile( InputStream inStream, String fileName ) throws IOException {
		File file = new File( fileName );
		OutputStream out = new FileOutputStream( file );
		byte buf[] = new byte[8192];
		int len;
		while( (len = inStream.read( buf )) > 0 ) {
			out.write( buf, 0, len );
		}
		out.close();
	}
}
