package is.rebbi.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * Tests for the ListUtilities
 */

public class TestListUtilities {

	@Test
	public void join() {
		String expected;
		String actual;

		expected = "Hugi";
		actual = ListUtilities.join( Arrays.asList( "Hugi" ), null, null );
		assertEquals( expected, actual );

		expected = "Kjartan og Strumparnir";
		actual = ListUtilities.join( Arrays.asList( "Kjartan", "Strumparnir" ), ", ", " og " );
		assertEquals( expected, actual );

		expected = "Hugi, Kjartan og Strumparnir";
		actual = ListUtilities.join( Arrays.asList( "Hugi", "Kjartan", "Strumparnir" ), ", ", " og " );
		assertEquals( expected, actual );
	}

	@Test
	public void join2() {
		String expected = "Hugi, Jón, Geiri";
		String actual = ListUtilities.join( Arrays.asList( "Hugi", "Jón", "Geiri" ), ", ", null );
		assertEquals( actual, expected );

		String expected2 = "Hugi, Jón og Geiri";
		String actual2 = ListUtilities.join( Arrays.asList( "Hugi", "Jón", "Geiri" ), ", ", " og " );
		assertEquals( actual2, expected2 );

		String expected3 = "Hugi, Jón";
		String actual3 = ListUtilities.join( Arrays.asList( "Hugi", "Jón" ), ", ", null );
		assertEquals( actual3, expected3 );

		String expected4 = "Hugi og Jón";
		String actual4 = ListUtilities.join( Arrays.asList( "Hugi", "Jón" ), ", ", " og " );
		assertEquals( actual4, expected4 );

		String expected5 = "Hugi";
		String actual5 = ListUtilities.join( Arrays.asList( "Hugi" ), ", ", null );
		assertEquals( actual5, expected5 );

		String expected6 = "Hugi";
		String actual6 = ListUtilities.join( Arrays.asList( "Hugi" ), ", ", " og " );
		assertEquals( actual6, expected6 );
	}
}