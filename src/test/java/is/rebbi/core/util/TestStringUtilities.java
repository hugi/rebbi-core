package is.rebbi.core.util;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

/**
 * Tests for StringUtilities.
 */

public class TestStringUtilities {

	private static final String TEST_FILE_NAME = "TestUSStringUtilities.txt";
	private static final String TEST_FILE_PATH = TestStringUtilities.class.getResource( TEST_FILE_NAME ).getPath();

	@Test
	public void stringHasValue() {
		assertFalse( StringUtilities.hasValue( "" ) );
		assertFalse( StringUtilities.hasValue( null ) );
		assertTrue( StringUtilities.hasValue( " " ) );
		assertTrue( StringUtilities.hasValue( "testString" ) );
	}

	@Test
	public void stringHasValueTrimmed() {
		assertFalse( StringUtilities.hasValueTrimmed( " " ) );
		assertTrue( StringUtilities.hasValueTrimmed( "1234" ) );
		assertFalse( StringUtilities.hasValueTrimmed( null ) );
		assertTrue( StringUtilities.hasValueTrimmed( "null" ) );
	}

	@Test
	public void padLeft() {
		assertEquals( "*asdf", StringUtilities.padLeft( "asdf", "*", 5 ) );
		assertEquals( "asdfasdf", StringUtilities.padLeft( "asdfasdf", "*", 5 ) );
		assertEquals( " asdf", StringUtilities.padLeft( "asdf", " ", 5 ) );
		assertEquals( " asdf asdf", StringUtilities.padLeft( "asdf asdf", " ", 10 ) );
	}

	@Test
	public void padRight() {
		assertEquals( "asdf ", StringUtilities.padRight( "asdf", " ", 5 ) );
		assertEquals( "asdfasdf", StringUtilities.padRight( "asdfasdf", " ", 5 ) );
		assertEquals( "asdf*", StringUtilities.padRight( "asdf", "*", 5 ) );
		assertEquals( "asdfasdf", StringUtilities.padRight( "asdfasdf", "*", 5 ) );
		assertEquals( "asdf ", StringUtilities.padRight( "asdf", " ", 5 ) );
		assertEquals( "asdf asdf ", StringUtilities.padRight( "asdf asdf", " ", 10 ) );
	}

	@Test
	public void writeStringToFileUsingEncoding() {
		File testFile = new File( TestUtilities.documentPath( this.getClass(), "writeStringToFileUsingEncoding.txt" ) );
		StringUtilities.writeStringToFileUsingEncoding( "the lazy cat crawled under the hyper dog", testFile, "utf-8" );
		assertEquals( "eab01c6dd27ef0193bc899b52b51919", TestUtilities.streamDigest( TEST_FILE_PATH ) );
	}

	@Test
	public void readStringFromURLUsingEncoding() {

		try {
			String retString = StringUtilities.readStringFromFileUsingEncoding( new File( TEST_FILE_PATH ), "utf-8" );
			assertEquals( "the lazy cat crawled under the hyper dog", retString );
		}
		catch( Exception e ) {
			fail( e.getMessage() );
		}
	}

	@Test
	public void replace() {
		assertThrows( NullPointerException.class, () -> {
			StringUtilities.replace( "smu", null, "bla" );
		} );

		assertThrows( NullPointerException.class, () -> {
			StringUtilities.replace( "smu", "bla", null );
		} );

		assertNull( StringUtilities.replace( null, "s", "d" ), "addf" );
		assertEquals( StringUtilities.replace( "asdf", "s", "d" ), "addf" );
		assertEquals( StringUtilities.replace( "aSdf", "S", "d" ), "addf" );
		assertEquals( StringUtilities.replace( "hyper dog", "hyper dog", "lazy cat" ), "lazy cat" );
	}

	@Test
	public void readStringFromFileUsingEncoding() {

		try {
			String retString = StringUtilities.readStringFromFileUsingEncoding( new File( TEST_FILE_PATH ), "utf-8" );
			assertEquals( "the lazy cat crawled under the hyper dog", retString );
		}
		catch( Exception e ) {
			fail( e.getMessage() );
		}
	}

	@Test
	public void abbreviateWithTrailer() {
		assertEquals( null, StringUtilities.abbreviate( null, 5 ) );
		assertEquals( "th...", StringUtilities.abbreviate( "the lazy cat crawled under the hyper dog", 5, "..." ) );
		assertEquals( "...", StringUtilities.abbreviate( "the lazy cat crawled under the hyper dog", 0, "..." ) );
		assertEquals( "", StringUtilities.abbreviate( "", 5, "..." ) );
	}

	@Test
	public void constructURLStringWithParameters() {
		Map<String, Object> params = new HashMap<String, Object>();
		assertEquals( StringUtilities.constructURLStringWithParameters( null, params, true ), "" );
		assertEquals( StringUtilities.constructURLStringWithParameters( "", null, true ), "" );
		assertEquals( StringUtilities.constructURLStringWithParameters( "www.us.is", params, true ), "www.us.is" );
		params.put( "a", "b" );
		assertEquals( StringUtilities.constructURLStringWithParameters( "www.us.is", params, true ), "www.us.is?a=b" );
		params.put( "æ", "ð" );
		assertEquals( StringUtilities.constructURLStringWithParameters( "www.us.is", params, true ), "www.us.is?a=b&amp;æ=ð" );
		assertEquals( StringUtilities.constructURLStringWithParameters( "www.us.is", params, false ), "www.us.is?a=b&æ=ð" );
	}

	@Test
	public void capitalize() {
		String original;
		String actual;
		String expected;

		original = "hugi þórðarson";
		actual = StringUtilities.capitalize( original );
		expected = "Hugi þórðarson";
		assertEquals( expected, actual );

		original = "þjóðarþýðingin";
		actual = StringUtilities.capitalize( original );
		expected = "Þjóðarþýðingin";
		assertEquals( expected, actual );
	}

	private Map<String, String> _stringsToTest;

	private Map<String, String> stringsToTest() {
		if( _stringsToTest == null ) {
			Map<String, String> d = new HashMap<String, String>();
			d.put( "Þjóðarþýðingin", "Thjodarthydingin" );
			d.put( "Magnús Jónatansson", "Magnus-Jonatansson" );
			d.put( "bárður bjöllusauður", "Bardur-Bjollusaudur" );
			d.put( "Júllablóð bellibrögðaávarp", "Jullablod-Bellibrogdaavarp" );
			_stringsToTest = d;
		}

		return _stringsToTest;
	}
}