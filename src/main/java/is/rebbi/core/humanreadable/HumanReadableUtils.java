package is.rebbi.core.humanreadable;

import java.util.ArrayList;
import java.util.List;

public class HumanReadableUtils {

	public static String toStringHuman( Object object ) {

		if( object == null ) {
			return null;
		}

		if( object instanceof List ) {
			final List<String> result = new ArrayList<>();

			for( Object each : ((List)object) ) {
				result.add( toStringHuman( each ) );
			}

			return String.join( ", ", result );
		}

		if( object instanceof HumanReadable ) {
			return ((HumanReadable)object).toStringHuman();
		}

		return object.toString();
	}
}