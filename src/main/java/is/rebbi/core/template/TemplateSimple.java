package is.rebbi.core.template;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import is.rebbi.core.util.StringUtilities;

/**
 * A very simple template system
 * The templates used are strings with variables of the form ${variableName}
 *
 * Example use:
 *
 * <code>
 * 		String templateString = "The quick ${color} fox jumps over a lazy ${lazyAnimal}";
 * 		USTemplate template = new USTemplateSimple();
 * 		template.setTemplateString( templateString );
 * 		template.put( "color", "brown" );
 * 		template.put( "lazyAnimal", "dog" );
 * 		String result = template.parse();
 * </code>
 *
 * Will return: The quick brown fox jumps over a lazy dog
 */

@Deprecated
public class TemplateSimple extends Template {

	/**
	 * The Pattern used to catch variables in the template String.
	 */
	private static final Pattern VAR_PATTERN = Pattern.compile( "\\$\\{.*?\\}" );

	/**
	 * Indicates if we want to remove unset variables from the template.
	 */
	private boolean _removeUnsetVariablesFromTemplate = true;

	/**
	 * @return The template with variables replaced with values from the value map.
	 */
	@Override
	public String parse() {
		String result = templateString();
		Matcher m = VAR_PATTERN.matcher( templateString() );

		while( m.find() ) {
			String group = m.group();
			String key = group.substring( 2, group.length() - 1 );
			Object value = variables().get( key );

			if( value == null && _removeUnsetVariablesFromTemplate ) {
				value = "";
			}

			if( value != null ) {
				result = StringUtilities.replace( result, group, value.toString() );
			}
		}

		return result;
	}

	/**
	 * Indicates if we want to remove unset variables from the template.
	 * The default value is [true].
	 */
	public void setRemoveUnsetVariablesFromTemplate( boolean value ) {
		_removeUnsetVariablesFromTemplate = value;
	}
}