package is.rebbi.core.formatters;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.util.Locale;

/**
 * Formats seconds and outputs a string formatted for days, hours, minutes and seconds.
 */

public class DurationFormatterDecimal extends Format {

	/**
	 * The pattern used to format decimal numbers.
	 */
	private static final String ICELANDIC_DECIMAL_FORMAT_PATTERN = "#,###.000";

	/**
	 * Icelandic formatting symbols.
	 */
	private static final DecimalFormatSymbols ICELANDIC_DECIMAL_FORMAT_SYMBOLS = new DecimalFormatSymbols( Locale.of( "is" ) );

	@Override
	public StringBuffer format( Object obj, StringBuffer toAppendTo, FieldPosition pos ) {

		if( obj != null ) {
			double seconds = ((Number)obj).doubleValue();

			if( seconds != 0 ) {
				double hours = seconds / 60d / 60d;
				toAppendTo.append( formatDouble( hours, 0, 2, false ) );
			}
		}

		return toAppendTo;
	}

	@Override
	public Object parseObject( String source, ParsePosition pos ) {
		throw new RuntimeException( "Parsing is not supported" );
	}

	/**
	 * Formats double into human readable string representation of it.
	 *
	 * @param 	number 				The number to be formated
	 * @param 	nrOfDecimalPlaces 	Number of decimal places to round to
	 * @param 	forceDecimalPlaces 	Should the decimal places be forced, e.g. 1.000,00
	 * @return 	A formated String representation of the double number
	 */
	private static String formatDouble( double number, int nrOfDecimalPlaces, int maxNrOfDecimalPlaces, boolean forceDecimalPlaces ) {
		DecimalFormat fmt = new DecimalFormat( ICELANDIC_DECIMAL_FORMAT_PATTERN, ICELANDIC_DECIMAL_FORMAT_SYMBOLS );

		if( forceDecimalPlaces ) {
			fmt.setMinimumFractionDigits( maxNrOfDecimalPlaces );
		}
		else {
			fmt.setMinimumFractionDigits( nrOfDecimalPlaces );
		}
		fmt.setMaximumFractionDigits( maxNrOfDecimalPlaces );

		String formatedNumber = fmt.format( number );

		if( formatedNumber.startsWith( ICELANDIC_DECIMAL_FORMAT_SYMBOLS.getDecimalSeparator() + "" ) ) {
			formatedNumber = "0" + formatedNumber;
		}

		return formatedNumber;
	}
}