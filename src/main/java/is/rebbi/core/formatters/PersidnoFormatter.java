package is.rebbi.core.formatters;

import is.rebbi.core.util.PersidnoUtilities;

import java.text.FieldPosition;
import java.text.ParsePosition;

/**
 * A formatter for formatting persidnos for display (inserts a dash after the sixth digit).
 */

public class PersidnoFormatter extends java.text.Format {

	/**
	 * Attempts to format a persidno to human readable format.
	 */
	@Override
	public StringBuffer format( Object persidno, StringBuffer toAppendTo, FieldPosition pos ) {

		if( persidno instanceof String ) {
			toAppendTo.append( PersidnoUtilities.formatPersidno( (String)persidno ) );
		}

		return toAppendTo;
	}

	/**
	 * @return A persidno on the standard format (no delimiters, just a 10 number string).
	 */
	@Override
	public Object parseObject( String source, ParsePosition status ) {
		String temp = source.substring( status.getIndex() );
		String result = PersidnoUtilities.cleanupPersidno( temp );
		status.setIndex( source.length() );
		return result;
	}
}