package is.rebbi.core.formatters;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import is.rebbi.core.util.DateUtilities;

public class RelativeDateFormatter extends Format {

	private static final DateTimeFormatter DEFAULT_TIME_FORMAT = DateTimeFormatter.ofPattern( "HH:mm" );
	private static final DateTimeFormatter DEFAULT_DATE_FORMAT = DateTimeFormatter.ofPattern( "d.M.yyyy" );

	private boolean _showToday = true;
	private boolean _showTime;

	private DateTimeFormatter _overrideDateFormat = null;

	@Override
	public StringBuffer format( Object obj, StringBuffer toAppendTo, FieldPosition pos ) {

		if( obj != null ) {
			toAppendTo.append( dateToRelativeString( obj, LocalDate.now().atStartOfDay(), _showToday, _showTime, dateFormat(), timeFormat() ) );
		}

		return toAppendTo;
	}

	public void setShowToday( boolean showToday ) {
		_showToday = showToday;
	}

	public void setShowTime( boolean showTime ) {
		_showTime = showTime;
	}

	public void setDefaultDateTimeFormatter( DateTimeFormatter format ) {
		_overrideDateFormat = format;
	}

	@Override
	public Object parseObject( String source, ParsePosition pos ) {
		throw new RuntimeException( "Parsing is not supported" );
	}

	private static String dateToRelativeString( Object object, LocalDateTime relativeToDate, boolean showToday, boolean showTime, DateTimeFormatter dateFormat, DateTimeFormatter timeFormat ) {

		LocalDateTime dateToFormat;

		if( object instanceof Date d ) {
			dateToFormat = DateUtilities.toLocalDateTime( d );
		}
		else if( object instanceof LocalDate ld ) {
			dateToFormat = ld.atStartOfDay(); // FIXME: This is horrible. Format LocalDates without the time
		}
		else if( object instanceof LocalDateTime ldt ) {
			dateToFormat = ldt;
		}
		else {
			throw new RuntimeException( "Object is not of a known date type: " + object.getClass() );
		}

		if( isToday( dateToFormat ) ) {
			final List<String> elements = new ArrayList<>();

			if( showToday ) {
				elements.add( "Í dag" );
			}

			if( showToday && showTime ) {
				elements.add( " kl. " );
			}

			if( showTime ) {
				elements.add( timeFormat.format( dateToFormat ) );
			}

			return String.join( " ", elements );
		}

		relativeToDate = relativeToDate.minusDays( 1 );

		if( dateToFormat.compareTo( relativeToDate ) >= 0 ) {
			StringBuilder b = new StringBuilder();
			b.append( "Í gær" );

			if( showTime ) {
				b.append( " kl. " );
				b.append( timeFormat.format( dateToFormat ) );
			}

			return b.toString();
		}

		relativeToDate = relativeToDate.minusDays( 1 );

		if( dateToFormat.compareTo( relativeToDate ) >= 0 ) {
			StringBuilder b = new StringBuilder();
			b.append( "Í fyrradag" );

			if( showTime ) {
				b.append( " kl. " );
				b.append( timeFormat.format( dateToFormat ) );
			}

			return b.toString();
		}

		return dateFormat.format( dateToFormat );
	}

	private DateTimeFormatter dateFormat() {
		if( _overrideDateFormat != null ) {
			return _overrideDateFormat;
		}

		return DEFAULT_DATE_FORMAT;
	}

	private DateTimeFormatter timeFormat() {
		return DEFAULT_TIME_FORMAT;
	}

	/**
	 * @return true if the given date is today, otherwise false
	 */
	static boolean isToday( LocalDateTime candidate ) {
		final LocalDateTime today = LocalDateTime.now();

		if( today.getYear() != candidate.getYear() ) {
			return false;
		}

		if( today.getDayOfYear() != candidate.getDayOfYear() ) {
			return false;
		}

		return true;
	}
}