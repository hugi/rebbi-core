package is.rebbi.core.formatters;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;

/**
 * Wraps a formatter to make it null safe.
 */

public class FormatterWrapperNullSafe extends Format {

	private Format _format;

	public FormatterWrapperNullSafe( Format format ) {

		if( format == null ) {
			throw new IllegalArgumentException( "[format] cannot be null" );
		}

		_format = format;
	}

	@Override
	public StringBuffer format( Object object, StringBuffer toAppendTo, FieldPosition pos ) {

		if( object == null ) {
			return toAppendTo;
		}

		return _format.format( object, toAppendTo, pos );
	}

	@Override
	public Object parseObject( String source, ParsePosition pos ) {
		return _format.parseObject( source, pos );
	}
}