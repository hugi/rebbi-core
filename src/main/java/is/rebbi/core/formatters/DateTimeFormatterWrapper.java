package is.rebbi.core.formatters;

import java.text.FieldPosition;
import java.text.Format;
import java.text.ParsePosition;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.Objects;

/**
 * A null safe java.text.Format wrapper for a java.time.format.DateTimeFormatter
 */

public class DateTimeFormatterWrapper extends Format {

	private final DateTimeFormatter _formatter;

	public DateTimeFormatterWrapper( DateTimeFormatter formatter ) {
		Objects.requireNonNull( formatter );
		_formatter = formatter;
	}

	@Override
	public StringBuffer format( Object object, StringBuffer toAppendTo, FieldPosition pos ) {

		if( object != null ) {
			final String formattedValue = _formatter.format( (TemporalAccessor)object );
			toAppendTo.append( formattedValue );
		}

		return toAppendTo;
	}

	@Override
	public Object parseObject( String source, ParsePosition pos ) {
		return _formatter.parse( source, pos );
	}
}