package is.rebbi.core.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class PriceIndex {

	private static Map<YearMonth, BigDecimal> _values = read();

	// FIXME: Hacky stuff, just to keep track of the last yearMonth we have an index value for
	private static YearMonth _lastInsertedYearMonth;

	private static Map<YearMonth, BigDecimal> read() {
		final Map<YearMonth, BigDecimal> map = new HashMap<>();
		final String sourceFile = StringUtilities.readStringFromResource( "/visitala.csv" );

		for( final String recordString : sourceFile.split( "\n" ) ) {
			final String[] record = recordString.split( ";" );
			final YearMonth yearMonth = YearMonth.parse( record[0] );
			final BigDecimal value = new BigDecimal( record[1].substring( 0, 5 ) ); // That substring is because of crlf line endings in the file
			map.put( yearMonth, value );
			_lastInsertedYearMonth = yearMonth;
		}

		return map;
	}

	/**
	 * @return the index value for the given month
	 */
	public static BigDecimal get( final YearMonth yearMonth ) {
		return _values.get( yearMonth );
	}

	/**
	 * @return The fractional change in the price index between two periods (0.2% being 0.0020, for example)
	 */
	public static BigDecimal getChange( final YearMonth start, final YearMonth end ) {
		Objects.requireNonNull( start );
		Objects.requireNonNull( end );

		final BigDecimal startIndexValue = get( start );
		BigDecimal endIndexValue = get( end );

		// FIXME: If we don't have a value for the end, use the most recent known value
		if( endIndexValue == null ) {
			endIndexValue = _values.get( _lastInsertedYearMonth );
		}

		if( startIndexValue == null ) {
			// FIXME: If we don't have data for the given month, we silently return no change. Not ideal.
			return BigDecimal.ZERO;
		}

		// FIXME: Is a scale of 4 really enough/correct to represent the real change?
		return endIndexValue.divide( startIndexValue, 4, RoundingMode.HALF_UP ).subtract( BigDecimal.ONE );
	}

	public static BigDecimal getChange( final BigDecimal amount, final YearMonth start, final YearMonth end ) {
		final BigDecimal change = amount.multiply( PriceIndex.getChange( start, YearMonth.now() ) );
		return amount.add( change ).setScale( 0, RoundingMode.HALF_UP );
	}

	/*
	public static String main( String[] args ) {
		Number scale = new Integer( 1 );

		switch( scale.intValue() ) {
			case 3: return "Þessi fannst vel hér";
			case 4: return "Þessi var öflugur";
			case 5: return "Hér leikur allt á reiðiskjálfi";
			case 6:
			case 7: {
				hringjaVekjaraklukkunniHjáBoga();
				return "Fréttir í sjónvarpi kl. " + (LocalDateTime.now().getHour() + 1);
			}
		}

		return "bla";
	}

	private static void hringjaVekjaraklukkunniHjáBoga() {
		// TODO Auto-generated method stub
	}
	*/
}