package is.rebbi.core.util;

import java.util.ArrayList;
import java.util.List;

/**
 * Allows the programmer to do various things with objects that implement the Hierarchy interface
 */

public class HierarchyUtilities {

	/**
	 * Indicates if the node has subnodes.
	 */
	public static <E extends Hierarchy<E>> boolean hasChildren( Hierarchy<E> object ) {
		return ListUtilities.hasObjects( object.children() );
	}

	/**
	 * Indicates if the node is at the top of the hiearchy (has no parent).
	 */
	public static <E extends Hierarchy<E>> boolean isRoot( Hierarchy<E> object ) {
		return object.parent() == null;
	}

	/**
	 * Returns nodes at the same level as this one in the hierarchy.
	 */
	public static <E extends Hierarchy<E>> List<E> siblings( E object ) {

		if( isRoot( object ) ) {
			final List<E> list = new ArrayList<E>();
			list.add( object );
			return list;
		}

		return (object.parent()).children();
	}

	/**
	 * Returns an array with all nodes below this one in the hierarchy.
	 *
	 * @param object the SWHierarchy object
	 * @param includingSelf indicates if the topLevePage should be included in the array.
	 */
	public static <E extends Hierarchy<E>> List<E> everyChild( E object, boolean includingSelf ) {

		final List<E> tempArray = new ArrayList<E>();

		if( includingSelf ) {
			tempArray.add( object );
		}

		if( hasChildren( object ) ) {
			for( E a : object.children() ) {

				tempArray.add( a );

				if( hasChildren( a ) ) {
					tempArray.addAll( everyChild( a, true ) );
				}
			}
		}

		return tempArray;
	}

	/**
	 * Indicates if child is a subnode of parent.
	 *
	 * @param includingSelf indicates if child should be checked against itself as well.
	 */
	public static <E extends Hierarchy<E>> boolean isParentNodeOfNode( E parent, E child, boolean includingSelf ) {
		return everyParentNode( child, includingSelf ).contains( parent );
	}

	/**
	 * Tells us if the specified page owes inheritance to the specified page
	 *
	 * @param child the page to check against
	 * @param parent
	 * @param includingTopLevel Indicates if the object should be checked against itself as well as it's children.
	 */
	public static <E extends Hierarchy<E>> boolean isChildOfNode( E child, E parent, boolean includingTopLevel ) {
		return isParentNodeOfNode( parent, child, includingTopLevel );
	}

	/**
	 * Returns the root node of the hierarchy.
	 *
	 * @param object The object to find the root for.
	 */
	public static <E extends Hierarchy<E>> E root( final E object ) {

		E h = object;

		while( !isRoot( h ) ) {
			h = h.parent();
		}

		return h;
	}

	/**
	 * Returns an array of all parent pages. includeSelf indicates if the calling page should be included.
	 * Order of the array starts with the given page and then walks upward, ending with the top level node.
	 */
	public static <E extends Hierarchy<E>> List<E> everyParentNode( final E object, final boolean includeSelf ) {

		if( object == null ) {
			return new ArrayList<>();
		}

		E h = object;

		final List<E> list = new ArrayList<E>();

		if( includeSelf ) {
			list.add( h );
		}

		while( h.parent() != null ) {
			h = h.parent();
			list.add( h );
		}

		return list;
	}

	/**
	 * Returns the page at the specified index in the parent page hierarchy. 0 is the front page of the site, 1 is the subpage of that page etc.
	 */

	public static <E extends Hierarchy<E>> E parentNodeAtLevel( final E object, final int level ) {
		final List<E> anArray = everyParentNode( object, true );
		return anArray.get( anArray.size() - level );
	}
}