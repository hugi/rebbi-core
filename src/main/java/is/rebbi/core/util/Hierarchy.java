package is.rebbi.core.util;

import java.util.List;

/**
 * Hierarchy can be implemented by any class to take advantage of the utility methods in USHierarcyUtilities.
 */

public interface Hierarchy<E extends Hierarchy<E>> {

	/**
	 * This node's parent node
	 */
	public E parent();

	/**
	 * This node's child nodes
	 */
	public List<E> children();
}