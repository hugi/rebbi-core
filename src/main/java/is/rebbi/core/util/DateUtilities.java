package is.rebbi.core.util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * Various utility methods for handling dates.
 */

public class DateUtilities {

	/**
	 * Calculates the age of a person given a birthdate and another date.
	 *
	 * @param birthDate The birthdate of the person to check.
	 * @param date The date at which we want to know the person's age. If null, we assume you want the current age.
	 * @return The person's age at [date]
	 */
	public static Integer ageAtDate( final LocalDate birthdate, final LocalDate date ) {

		int ageInYears = date.getYear() - birthdate.getYear();

		if( (birthdate.getMonthValue() == date.getMonthValue()) && (birthdate.getDayOfMonth() <= date.getDayOfMonth()) ) {
			return ageInYears;
		}
		else if( birthdate.getMonthValue() < date.getMonthValue() ) {
			return ageInYears;
		}

		return ageInYears - 1;
	}

	public static LocalDate toLocalDate( final Date date ) {

		if( date == null ) {
			return null;
		}

		return date.toInstant().atZone( ZoneId.systemDefault() ).toLocalDate();
	}

	public static LocalDateTime toLocalDateTime( final Date date ) {

		if( date == null ) {
			return null;
		}

		return date.toInstant().atZone( ZoneId.systemDefault() ).toLocalDateTime();
	}

	public static Date toDate( final LocalDate localDate ) {

		if( localDate == null ) {
			return null;
		}

		return toDate( localDate.atStartOfDay() );
	}

	public static Date toDate( final LocalDateTime localDateTime ) {

		if( localDateTime == null ) {
			return null;
		}

		return Date.from( localDateTime.atZone( ZoneId.systemDefault() ).toInstant() );
	}
}