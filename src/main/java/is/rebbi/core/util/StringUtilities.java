package is.rebbi.core.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UncheckedIOException;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;

/**
 * Various utility methods for handling strings.
 */

public class StringUtilities extends Object {

	/**
	 * Name of our default encoding.
	 */
	private static final String UTF_8 = "UTF-8";

	/**
	 * No instances created, ever.
	 */
	private StringUtilities() {}

	/**
	 * This method return true if a string is not null, and not equal to the empty String ""
	 *
	 * @param string The string to check
	 */
	public static final boolean hasValue( String string ) {
		return string != null && string.length() > 0;
	}

	/**
	 * Returns true if string is not null, not empty, and contains something else that white spaces
	 *
	 * @param string The string to check
	 */
	public static boolean hasValueTrimmed( String string ) {

		if( !hasValue( string ) ) {
			return false;
		}

		return hasValue( string.trim() );
	}

	/**
	 * This method will adjust a String to a certain length. If the string is
	 * too short, it pads it on the left. If the string is too long,
	 * it will cut of the left end of it.
	 *
	 * Created to make creation of fixed length string easier.
	 */
	public static String padLeft( String string, String padString, int desiredLength ) {

		String str = (string != null) ? string : "";
		StringBuilder strBuff = new StringBuilder();

		int strLength = str.length();

		if( desiredLength > 0 && desiredLength > strLength ) {
			for( int i = 0; i <= desiredLength; i++ ) {
				if( i > strLength ) {
					strBuff.append( padString );
				}
			}
		}

		strBuff.append( str );
		return strBuff.toString();
	}

	/**
	 * This method will adjust a String to a certain length. If the string is
	 * too short, it pads it on the right. If the string is too long,
	 * it will cut of the right end of it.
	 *
	 * Created to make creation of fixed length string easier.
	 */
	public static String padRight( String string, String padString, int desiredLength ) {

		String str = (string != null) ? string : "";
		StringBuilder strBuff = new StringBuilder();

		int strLength = str.length();

		if( desiredLength > 0 && desiredLength > strLength ) {
			for( int i = 0; i <= desiredLength; i++ ) {
				if( i > strLength ) {
					strBuff.append( padString );
				}
			}
		}

		strBuff.insert( 0, string );
		return strBuff.toString();
	}

	/**
	 * Writes the given string to a file, using the given encoding.
	 */
	public static void writeStringToFileUsingEncoding( String sourceString, File destination, String encoding ) {

		if( encoding == null ) {
			encoding = UTF_8;
		}

		try {
			Files.writeString( destination.toPath(), sourceString, Charset.forName( encoding ) );
		}
		catch( IOException e ) {
			throw new UncheckedIOException( e );
		}
	}

	/**
	 * Fetches the given string from an URL, using the given encoding.
	 *
	 * @param sourceURL the URL to read from
	 * @param encoding The encoding of the data at the source URL.
	 * @return the string that was read
	 */
	public static String readStringFromURLUsingEncoding( String sourceURL, String encoding ) {
		Objects.requireNonNull( sourceURL );

		if( encoding == null ) {
			encoding = UTF_8;
		}

		try {
			final URL url = new URL( sourceURL );

			try( InputStream in = url.openStream()) {
				return new String( in.readAllBytes(), encoding );
			}
		}
		catch( IOException e ) {
			throw new UncheckedIOException( e );
		}
	}

	/**
	 * Reads a string from a file, using the given encoding.
	 */
	public static String readStringFromFileUsingEncoding( File sourceFile, String encoding ) {

		if( encoding == null ) {
			encoding = UTF_8;
		}

		try {
			return Files.readString( sourceFile.toPath(), Charset.forName( encoding ) );
		}
		catch( IOException e ) {
			throw new UncheckedIOException( e );
		}
	}

	/**
	 * @return The named resource as a string, relative to the root. Paths must start with a slash, indicating you're searching from the root.
	 */
	public static String readStringFromResource( final String resourcePath ) {
		try( InputStream is = StringUtilities.class.getResourceAsStream( resourcePath )) {
			return new String( is.readAllBytes(), "utf-8" );
		}
		catch( IOException e ) {
			throw new UncheckedIOException( e );
		}
	}

	/**
	 * Abbreviates a string if it exceeds length
	 */
	public static String abbreviate( String string, int length ) {
		return abbreviate( string, length, "" );
	}

	/**
	 * Abbreviates a string if it exceeds length and appends [trailer]
	 */
	public static String abbreviate( String string, int length, String trailer ) {

		if( string == null ) {
			return null;
		}

		if( trailer == null ) {
			trailer = "";
		}

		int trailerLen = trailer.length();
		int newLen = (length > trailerLen) ? length - trailerLen : length;

		if( (string.length() > length) && (newLen >= 0) ) {
			StringBuilder b = new StringBuilder();
			b.append( string.substring( 0, newLen ) );
			b.append( trailer );
			return b.toString();
		}

		return string;
	}

	/**
	 * Creates a complete url, starting with the base url, appending each parameter in query string format.
	 *
	 * @param baseURL The baseURL to use.
	 * @param parameters A dictionary of URL parameters to append to the base url.
	 * @param escapeAmpersands If true, parameters will escape the ampersand separating query parameters.
	 * @return A complete URL
	 */
	public static String constructURLStringWithParameters( String baseURL, Map<String, Object> parameters, boolean escapeAmpersands ) {

		StringBuilder b = new StringBuilder();

		if( hasValue( baseURL ) ) {
			b.append( baseURL );
		}

		if( parameters != null && parameters.size() > 0 ) {
			b.append( "?" );

			Iterator<String> i = parameters.keySet().iterator();

			while( i.hasNext() ) {
				String nextKey = i.next();
				Object nextValue = parameters.get( nextKey );

				b.append( nextKey );
				b.append( "=" );

				if( nextValue != null ) {
					b.append( nextValue );
				}

				if( i.hasNext() ) {
					if( escapeAmpersands ) {
						b.append( "&amp;" );
					}
					else {
						b.append( "&" );
					}
				}
			}
		}

		return b.toString();
	}

	/**
	 * Replaces a substring with another string in the buffer.
	 */
	public static String replace( final String sourceString, final String oldString, String newString ) {

		if( sourceString == null ) {
			return null;
		}

		Objects.requireNonNull( oldString );
		Objects.requireNonNull( newString );

		return sourceString.replace( oldString, newString );
	}

	/**
	 * Checks if the specified String contains only digits.
	 *
	 * @param string the string to check
	 * @return true if the string contains only digits, false otherwise
	 */
	public static boolean isDigitsOnly( String string ) {

		for( int i = string.length(); i-- > 0; ) {
			char c = string.charAt( i );
			if( !Character.isDigit( c ) ) {
				return false;
			}
		}

		return true;
	}

	/**
	 * Capitalizes the string.
	 */
	public static final String capitalize( String s ) {

		if( s == null || s.length() == 0 ) {
			return s;
		}

		StringBuilder b = new StringBuilder();
		b.append( s.substring( 0, 1 ).toUpperCase() );

		if( s.length() > 1 ) {
			b.append( s.substring( 1 ) );
		}

		return b.toString();
	}
}