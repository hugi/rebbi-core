package is.rebbi.core.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * Various utility methods for collections.
 */

public class ListUtilities {

	private static final Random RANDOM = new Random();

	private ListUtilities() {}

	/**
	 * @return True if the given collection is not null and contains objects.
	 */
	public static boolean hasObjects( Collection<?> c ) {
		return c != null && !c.isEmpty();
	}

	/**
	 * @param list The list to read
	 * @param <E> Types of objects in the list
	 * @return A random object from the specified array. If the array contains no objects or is null, null is returned.
	 */
	public static <E> E randomObject( List<E> list ) {

		if( !hasObjects( list ) ) {
			return null;
		}

		int randomIndex = RANDOM.nextInt( list.size() );
		return list.get( randomIndex );
	}

	/**
	 * @return A new array containing the specified number of random objects form the source array.
	 *
	 *         If the array is null, an empty array is returned. If the source array is shorter or equal in length to the count, the original array is returned randomized.
	 *
	 * @param array The array to read from.
	 * @param count The max size of the return array.
	 */
	public static <E> List<E> randomObjects( List<E> array, int count ) {

		if( !hasObjects( array ) ) {
			return new ArrayList<>();
		}

		if( count >= array.size() ) {
			return randomized( array );
		}

		List<E> originalArrayMutable = new ArrayList<>( array );
		List<E> result = new ArrayList<>();

		for( int i = count; i > 0; i-- ) {
			E randomObject = randomObject( originalArrayMutable );
			result.add( randomObject );
			originalArrayMutable.remove( randomObject );
		}

		return result;
	}

	/**
	 * Randomizes the objects in an array
	 */
	public static <E> List<E> randomized( List<E> array ) {

		if( !hasObjects( array ) ) {
			return new ArrayList<>();
		}

		List<E> originalArray = new ArrayList<>( array );
		List<E> resultArray = new ArrayList<>();

		while( originalArray.size() > 0 ) {
			E o = randomObject( originalArray );
			resultArray.add( o );
			originalArray.remove( o );
		}

		return resultArray;
	}

	/**
	 * @param list the list to join
	 * @param separator The separator to use
	 * @param lastSeparator The last separator inserted. If null, [separator] is used.
	 * @return The List joined using the given separator.
	 */
	public static String join( List<?> list, String separator, String lastSeparator ) {

		if( list == null ) {
			return null;
		}

		if( separator == null ) {
			separator = ",";
		}

		if( lastSeparator == null ) {
			lastSeparator = separator;
		}

		StringBuilder b = new StringBuilder();

		for( int i = 0; i < list.size(); i++ ) {
			Object o = list.get( i );

			b.append( o );

			if( i == list.size() - 2 ) {
				b.append( lastSeparator );
			}
			else if( i < list.size() - 2 ) {
				b.append( separator );
			}
		}

		return b.toString();
	}

	/**
	 * @param list The list
	 * @param list object after the object to return.
	 * @return Previous object in the given List, if available. Otherwise, null.
	 */
	public static <L> L previousObject( List<L> list, L object ) {
		int indexOfThis = list.indexOf( object );

		if( indexOfThis == 0 ) {
			return null;
		}

		return list.get( indexOfThis - 1 );
	}

	/**
	 * @param list The list
	 * @param list object before the object to return.
	 * @return Next object in the given List, if available. Otherwise, null.
	 */
	public static <L> L nextObject( List<L> list, L object ) {
		int indexOfThis = list.indexOf( object );

		if( indexOfThis == list.size() - 1 ) {
			return null;
		}

		return list.get( indexOfThis + 1 );
	}

	/**
	 * Returns the list unmodified if it contains [maxCount] or fewer items, otherwise
	 *
	 * @param list The list to work with
	 * @param maxCount The maximum number of items to return
	 *
	 * @return the first [maxCount] items from the given list
	 */
	public static <E> List<E> maxObjectsFromList( List<E> list, Integer maxCount ) {

		if( list == null || list.isEmpty() ) {
			return new ArrayList<>();
		}

		if( maxCount == null ) {
			return list;
		}

		if( maxCount < 0 ) {
			throw new IllegalArgumentException( "The parameter 'maxCount' must be a positive integer" );
		}

		if( list.size() <= maxCount ) {
			return list;
		}

		return list.subList( 0, maxCount );
	}
}